<!DOCTYPE html>
    <html lang:"en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>
                Document
            </title> 
            <style>
                .display{
                    width: 155px;
                    height: 30px;
                    background-color:#efeeee;
                    border-radius: 5px;
                    border: solid 1px black;
                    text-align: right;
                    font-size: 20px;
                }
                .btn{
                    width: 35px;
                    height: 35px;
                    background-color: #c7c7c7;
                    border-radius: 50%;
                    text-align: center;
                    float: left;
                    font-size: 30px;
                    margin-right: 5px;
                }
                .column{
                    margin-top: 5px;
                    width: 175px;
                    height: 35px;
                }
                .opera{
                    background-color: #9cdbf9;
                }
                .ac{
                    font-size: 20px;
                    background-color: #21d3aa;
                }
                .calculadora{
                    background-color: #f5d7d7;
                    padding: 10px;
                    height: 192px;
                    width: 156px;
                }
            </style>
        </head>
        <body>
            <div class="calculadora">
                <div class="display" id="display"></div>
                <div class="column">
                    <div class="btn" id="btn7" onclick="Numero(7)">7</div>
                    <div class="btn" id="btn8" onclick="Numero(8)">8</div>
                    <div class="btn" id="btn9" onclick="Numero(9)">9</div>
                    <div class="btn opera" id="btnx" onclick="btnx()">x</div>
                </div>    
                <div class="column">
                    <div class="btn" id="btn4" onclick="Numero(4)">4</div>
                    <div class="btn" id="btn5" onclick="Numero(5)">5</div>
                    <div class="btn" id="btn6" onclick="Numero(6)">6</div>
                    <div class="btn opera" id="btnMenos" onclick="btnMenos()">-</div>
                </div>    
                <div class="column">
                    <div class="btn" id="btn1" onclick="Numero(1)">1</div>
                    <div class="btn" id="btn2" onclick="Numero(2)">2</div>
                    <div class="btn" id="btn3" onclick="Numero(3)">3</div>
                    <div class="opera btn" id="btnMas" onclick="btnMas()">+</div>
                </div>    
            <div class="column">
                    <div class="btn ac opera" id="btnAC" onclick="btnAC()">AC</div>
                    <div class="btn" id="btn0" onclick="Numero(0)">0</div>
                    <div class="btn opera" id="btnEntre" onclick="btnEntre()">/</div>
                    <div class="btn opera" id="btnIgual" onclick="btnIgual()">=</div>
            </div>   
            </div>
            <script>
                var cadena1 = 0;
                var cadena2 = 0;
                var operacion = "";
                function Numero(item)
                {
                    if (operacion != "")
                    {
                        document.getElementById("display").innerHTML = "" + item;     
                    }
                    else
                    {
                        document.getElementById("display").innerHTML = document.getElementById("display").innerHTML + item;
                    }
                }
                function btnMas()
                {
                    if (cadena1 == 0)
                    {
                        cadena1 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = "";
                        operacion = "";
                        console.log("estado 1");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                    else{
                        operacion = "+";
                        cadena2 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = (cadena1 + cadena2);
                        cadena1 = (cadena1 + cadena2);
                        console.log("estado 2");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                }
                function btnMenos()
                {
                    if (cadena1 == 0)
                    {
                        cadena1 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = "";
                        operacion = "";
                        console.log("estado 1");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                    else{
                        operacion = "-";
                        cadena2 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = (cadena1 - cadena2);
                        cadena1 = (cadena1 - cadena2);
                        console.log("estado 2");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                }
                function btnx() 
                {
                    if (cadena1 == 0)
                    {
                        cadena1 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = "";
                        operacion = "";
                        console.log("estado 1");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                    else{
                        operacion = "*";
                        cadena2 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = (cadena1 * cadena2);
                        cadena1 = (cadena1 * cadena2);
                        console.log("estado 2");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                }
                function btnEntre()
                {
                    if (cadena1 == 0)
                    {
                        cadena1 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = "";
                        operacion = "";
                        console.log("estado 1");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                    else{
                        operacion = "/";
                        cadena2 = parseFloat(document.getElementById("display").innerHTML);
                        document.getElementById("display").innerHTML = (cadena1 / cadena2);
                        cadena1 = (cadena1 / cadena2);
                        console.log("estado 2");
                        console.log("cadena1: " + cadena1);
                        console.log("cadena2: " + cadena2);
                    }
                }
                function btnAC() 
                {
                    document.getElementById("display").innerHTML = "";   
                    operacion = "AC";
                    cadena1= 0;
                }
                function btnIgual()
                {
                    
                }
            </script>
        </body>
    </html>